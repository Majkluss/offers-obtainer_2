"""Tests for the Offers Obtainer application"""
import os
from app import app
from db.database import add_product

headers = {"Token": os.environ.get('API_TOKEN')}

SPICE_HARVESTER = {
    "id": "42424242-4242-4242-4242-424242424242",
    "name": "Test Spice Harvester",
    "description": "Spice Harvester obtains the Spice."
}

SAND_WORM_WITHOUT_ID = {
    "name": "Test Sand Worm",
    "description": "Sand Worm creates the Spice."
}

SAND_WORM_WRONG_ID = {
    "id": "42424242-4242-4242-4242-424242424243",
    "name": "Test Sand Worm",
    "description": "Sand Worm creates the Spice."
}

SAND_WORM = {
    "id": "42424242-4242-4242-4242-424242424242",
    "name": "Test Sand Worm",
    "description": "Sand Worm creates the Spice."
}


class TestsProducts:
    """Tests for products"""
    def test_get_product_without_token(self):
        """Test if user can get products without token"""
        with app.test_client() as client:
            response = client.get('/api/products')
            assert response.status_code == 401

    def test_create_product(self):
        """Test if user can create product"""
        with app.test_client() as client:
            with app.app_context():
                # Add product only to database, without register to Offer microservice
                add_product(SPICE_HARVESTER)
                response = client.get('/api/products',
                                      headers=headers)
                assert response.status_code == 200

    def test_get_product(self):
        """Test if user can get product"""
        with app.test_client() as client:
            response = client.get(f'/api/products/{SPICE_HARVESTER["id"]}',
                                  headers=headers)
            assert response.status_code == 200
            assert response.get_json() == SPICE_HARVESTER

    def test_update_product_missing_id(self):
        """Test if user can update product without ID"""
        with app.test_client() as client:
            response = client.patch('/api/products', headers=headers,
                                    json=SAND_WORM_WITHOUT_ID)
            assert response.status_code == 409

    def test_update_product_wrong_id(self):
        """Test if user can update product with wrong ID"""
        with app.test_client() as client:
            response = client.patch('/api/products', headers=headers,
                                    json=SAND_WORM_WRONG_ID)
            assert response.status_code == 404

    def test_update_product(self):
        """Test if user can update product"""
        with app.test_client() as client:
            response = client.patch('/api/products', headers=headers,
                                    json=SAND_WORM)
            assert response.status_code == 204

    def test_get_updated_product(self):
        """Test if user can get updated product"""
        with app.test_client() as client:
            response = client.get(f'/api/products/{SPICE_HARVESTER["id"]}',
                                  headers=headers)
            assert response.status_code == 200
            assert response.get_json() == SAND_WORM

    def test_delete_product(self):
        """Test if user can delete product"""
        with app.test_client() as client:
            response = client.delete('/api/products', headers=headers,
                                     json={"id": SPICE_HARVESTER["id"]})
            assert response.status_code == 204

    def test_get_deleted_product(self):
        """Test if user can get deleted product"""
        with app.test_client() as client:
            response = client.get(f'/api/products/{SPICE_HARVESTER["id"]}',
                                  headers=headers)
            assert response.status_code == 404
